/*jslint browser: true */

// Create a "splinter" namespace object - we'll put all of the spliter variables
// here, and when we're finished we will simply delete it, so that we "fade away
// without a trace".
var splinter = {};

// A convenience function to insert new script tags directly after this one (for
// the user's ease).
splinter.insertAfter = function (referenceNode, newNode) {
    "use strict";
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
};

// A function to retrieve the script text *synchronously* using XHR.
splinter.newScript = function (src, attrs) {
    "use strict";

    var attrstring = [],
        k;

    attrs = attrs || {};
    for (k in attrs) {
        attrstring = attrstring.concat(k + '="' + attrs[k] + '"');
    }
    if (attrstring.length > 0) {
        attrstring = " " + attrstring.join(" ");
    }

    document.write('<script src="' + src + '"' + attrstring + '></script>');
};

splinter.kill = function (x) {
    "use strict";
    x.parentNode.removeChild(x);
};

splinter.whoami = function () {
    "use strict";
    return document.getElementsByTagName("script")[document.getElementsByTagName("script").length - 1];
};
