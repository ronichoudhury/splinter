cmake_minimum_required(VERSION 2.8)
project(splinter NONE)
mark_as_advanced(CMAKE_INSTALL_PREFIX)

file(READ loader-preamble.js LOADER_PREAMBLE)
configure_file(load-all.js.in ${CMAKE_BINARY_DIR}/load-all.js)
configure_file(load-all.min.js.in ${CMAKE_BINARY_DIR}/load-all.min.js)

configure_file(load-d3.js ${CMAKE_BINARY_DIR}/load-d3.js COPYONLY)
configure_file(load-d3.min.js ${CMAKE_BINARY_DIR}/load-d3.min.js COPYONLY)
configure_file(load-jquery.js ${CMAKE_BINARY_DIR}/load-jquery.js COPYONLY)
configure_file(load-jquery.min.js ${CMAKE_BINARY_DIR}/load-jquery.min.js COPYONLY)
