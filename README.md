splinter
========

*"Our domain is the shadow. Stray from it reluctantly, but when you do, you must strike hard and fade away... without a trace."*

*--Master Splinter*

A mechanism for expanding a JavaScript "loader script" inclusion into a series of dependent scripts.
